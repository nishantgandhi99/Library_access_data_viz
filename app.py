import pandas as pd
import numpy as np
from numpy.ma.core import _check_mask_axis
import re

def fun(da):
    da = str(da)
    if da is not None:
        if da is not "":
            # dd/mm/yyy   hr:mm:ss AM/PM
            # print("full date:"+ da)
            arr = da.split(" ")
            # print("arr[1]:" + arr[len(arr)-1])
            arr1 = arr[len(arr)-1].split(":")
            # print("arr1[0]:" + arr1[0])
            timing = arr1[2][-2:]

            if timing=='PM':
                return int(arr1[0])+12
            else:
                return int(arr1[0])
        else:
            return 0
    else:
        return 0


df_full = pd.read_csv('Swipe_logs_2015.csv', low_memory=False, na_filter=False)

df_full['HOUR'] = df_full.apply(lambda x: fun(x['TRAN_DATE']), axis=1)
df_full = df_full[['HOUR','LEVEL','COLL','PROGRAM','TYPE']]
print(df_full.dtypes)
print(df_full.shape)


df_full_alum = df_full[df_full['TYPE']=='ALU']

# df_full_alum['HOUR'] = df_full_alum.apply(lambda x: fun(x['TRAN_DATE']), axis=1)

df_full_stud = df_full[df_full['TYPE']=='STU']
print(df_full.shape)


df_full_stud_ug = df_full_stud[df_full_stud['LEVEL']=='UG']
print(df_full_stud_ug.shape)

df_full_stud_gr = df_full_stud[df_full_stud['LEVEL']=='GR']
print(df_full_stud_gr.shape)


# print(df_full)
print("Alumni")
print(df_full_alum.groupby('HOUR').count())

print("UG")
print(df_full_stud_ug.groupby('HOUR').count())


print("GE")
print(df_full_stud_gr.groupby('HOUR').count())

# df_full_alum.to_csv('df_full_alum.csv',index=False)
# df_full_stud_ug.to_csv('df_full_stud_ug.csv', index=False)
# df_full_stud_gr.to_csv('df_full_stud_gr.csv', index=False)
